package com.tp5.dao;

import com.tp5.models.Author;
import com.tp5.models.Book;
import com.tp5.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDAO extends DAO<Book> {
    @Override
    public Book get(Integer id) {
        Book book = null;
        TypeDAO typeDAO = null;
        AuthorDAO authorDAO = null;

        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM books WHERE book_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        typeDAO = new TypeDAO();
                        authorDAO = new AuthorDAO();
                        book = new Book(
                                resultSet.getInt("book_id"),
                                resultSet.getString("title"),
                                resultSet.getDate("publication_date"),
                                typeDAO.get(resultSet.getInt("type_id")),
                                authorDAO.get(resultSet.getInt("author_id"))
                        );
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return book;
    }

    @Override
    public List<Book> getAll() {
        return null;
    }

    @Override
    public void create(Book obj) {

    }

    @Override
    public void update(Book obj) {

    }

    @Override
    public void delete(Book obj) {

    }
}
