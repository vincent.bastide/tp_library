package com.tp5.dao;

import com.tp5.models.User;

import java.sql.*;
import java.util.List;

public class UserDAO extends DAO<User> {
    @Override
    public User get(Integer id) {
        return null;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public void create(User obj) {
        if (obj != null) {
            try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
                final String SQL_QUERY = "INSERT INTO USERS(lastname, firstname, email, password) VALUES(?, ?, ?, ?)";
                try (PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                    statement.setString(1, obj.getLastname());
                    statement.setString(2, obj.getFirstname());
                    statement.setString(3, obj.getEmail());
                    statement.setString(4, obj.getPassword());
                    try {
                        statement.executeUpdate();
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void update(User obj) {

    }

    @Override
    public void delete(User obj) {

    }

    public User getByEmail(String email) {
        User user = null;
        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM USERS WHERE email=?";
            try(PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                statement.setString(1, email);
                try(ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        user = new User(
                                resultSet.getInt("user_id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getString("email"),
                                resultSet.getString("password")
                        );
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return user;
    }

    public User isValidLogin(String email, String password) {
        User user = this.getByEmail(email);

        if ((user != null) && !user.getPassword().equals(password)) {
            user = null;
        }
        return user;
    }
}
