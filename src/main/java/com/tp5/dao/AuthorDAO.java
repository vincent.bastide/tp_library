package com.tp5.dao;

import com.tp5.models.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthorDAO extends DAO<Author> {
    @Override
    public Author get(Integer id) {
        Author author = null;

        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM authors WHERE author_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if(resultSet.next()) {
                        author = new Author(
                                resultSet.getInt("author_id"),
                                resultSet.getString("lastname"),
                                resultSet.getString("firstname")
                        );
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return author;
    }

    @Override
    public List<Author> getAll() {
        List<Author> authors = null;

        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM authors";
            try (PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    authors = new ArrayList<Author>();

                    while(resultSet.next()) {
                        authors.add(new Author(
                                resultSet.getInt("author_id"),
                                resultSet.getString("lastname"),
                                resultSet.getString("firstname")
                        ));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return authors;
    }

    @Override
    public void create(Author obj) {

    }

    @Override
    public void update(Author obj) {

    }

    @Override
    public void delete(Author obj) {

    }
}
