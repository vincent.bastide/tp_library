package com.tp5.dao;

import com.tp5.models.Author;
import com.tp5.models.Book;
import com.tp5.models.Catalog;
import com.tp5.models.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CatalogDAO extends DAO<Catalog> {
    @Override
    public Catalog get(Integer id) {
        return null;
    }

    @Override
    public List<Catalog> getAll() {
        final String SQL_QUERY = "SELECT * FROM catalogs";
        List<Catalog> catalog = null;
        BookDAO bookDAO = null;

        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            try(PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                try(ResultSet resultSet = statement.executeQuery()) {
                    catalog = new ArrayList<Catalog>();
                    bookDAO = new BookDAO();

                    while(resultSet.next()) {
                        catalog.add(new Catalog(
                                bookDAO.get(resultSet.getInt("book_id")),
                                resultSet.getInt("nb_available")
                        ));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return catalog;
    }

    @Override
    public void create(Catalog obj) {

    }

    @Override
    public void update(Catalog obj) {

    }

    @Override
    public void delete(Catalog obj) {

    }

    public List<Catalog> getWithFilters(Integer author_id, Integer type_id, Boolean available) {
        List<Catalog> catalog = null;
        TypeDAO typeDAO = null;
        AuthorDAO authorDAO = null;
        boolean previousParameter = false;
        String sqlQuery = "SELECT * " +
                "FROM catalogs " +
                "NATURAL JOIN books B " +
                "INNER JOIN types T ON B.type_id = T.type_id " +
                "INNER JOIN authors A ON B.author_id = A.author_id";

        if (author_id != null) {
            sqlQuery += " WHERE B.author_id = ?";
            previousParameter = true;
        }
        if (type_id != null) {
            if (previousParameter) {
                sqlQuery += " AND";
            } else {
                sqlQuery += " WHERE";
            }
            sqlQuery += " B.type_id = ?";
        }

        if (available != null) {
            if (previousParameter) {
                sqlQuery += " AND";
            } else {
                sqlQuery += " WHERE";
            }

            sqlQuery += " nb_available";
            if (available) {
                sqlQuery += ">0";
            } else {
                sqlQuery += "=0";
            }
        }

        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            try(PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
                if (author_id != null) {
                    statement.setInt(1, author_id);
                    if (type_id != null) {
                        statement.setInt(2, type_id);
                    }
                } else if (type_id != null) {
                    statement.setInt(1, type_id);
                }

                try(ResultSet resultSet = statement.executeQuery()) {
                    catalog = new ArrayList<Catalog>();
                    typeDAO = new TypeDAO();
                    authorDAO = new AuthorDAO();

                    while(resultSet.next()) {
                        catalog.add(new Catalog(
                                new Book(
                                        resultSet.getInt("book_id"),
                                        resultSet.getString("title"),
                                        resultSet.getDate("publication_date"),
                                        typeDAO.get(resultSet.getInt("type_id")),
                                        authorDAO.get(resultSet.getInt("author_id"))
                                ),
                                resultSet.getInt("nb_available")
                        ));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return catalog;
    }
}
