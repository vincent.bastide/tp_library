package com.tp5.listeners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;


@WebListener
public class RequestListener implements ServletRequestListener {
    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        if (request.getSession(false) != null) {
            Integer isUserConnected = (Integer) request.getSession(false).getAttribute("user_id");
            request.setAttribute("isConnected", isUserConnected);
        }

    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
    }
}
