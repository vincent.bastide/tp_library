package com.tp5.models;

public class Catalog {
    private Book book;
    private int nbAvailable;

    public Catalog(Book book, int nbAvailable) {
        this.book = book;
        this.nbAvailable = nbAvailable;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getNbAvailable() {
        return nbAvailable;
    }

    public void setNbAvailable(int nbAvailable) {
        this.nbAvailable = nbAvailable;
    }
}
