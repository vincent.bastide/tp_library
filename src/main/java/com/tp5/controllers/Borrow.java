package com.tp5.controllers;

import com.tp5.dao.BookDAO;
import com.tp5.dao.BorrowingDAO;
import com.tp5.dao.UserDAO;
import com.tp5.models.Book;
import com.tp5.models.Borrowing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(urlPatterns = "/borrow")
public class Borrow extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer book_id = (!request.getParameter("id").isEmpty()) ? Integer.parseInt(request.getParameter("id")) : null;
        BookDAO bookDAO = null;

        if (book_id != null) {
            bookDAO = new BookDAO();

            Book book = bookDAO.get(book_id);
            request.setAttribute("book", book);
        }

        this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/borrow.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BorrowingDAO borrowingDAO = null;
        UserDAO userDAO = null;
        BookDAO bookDAO = null;
        Borrowing borrow = null;
        Integer book_id = Integer.parseInt(request.getParameter("book_id"));

        if (book_id != null) {
            borrowingDAO = new BorrowingDAO();
            userDAO = new UserDAO();
            bookDAO = new BookDAO();
            borrow = new Borrowing(
                    null,
                    userDAO.get((Integer) request.getSession(false).getAttribute("user_id")),
                    bookDAO.get(book_id),
                    new Date(),
                    null
            );

            borrowingDAO.create(borrow);
        }
        response.sendRedirect("/catalog");
    }
}
