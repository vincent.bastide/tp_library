package com.tp5.controllers;

import com.tp5.dao.AuthorDAO;
import com.tp5.dao.CatalogDAO;
import com.tp5.dao.TypeDAO;
import com.tp5.models.Author;
import com.tp5.models.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/catalog")
public class Catalog extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CatalogDAO catalogDAO = new CatalogDAO();
        AuthorDAO authorDAO = new AuthorDAO();
        TypeDAO typeDAO = new TypeDAO();

        List<com.tp5.models.Catalog> catalog = catalogDAO.getAll();
        List<Author> authors = authorDAO.getAll();
        List<Type> types = typeDAO.getAll();

        request.setAttribute("title", "Catalogue");
        request.setAttribute("catalog", catalog);
        request.setAttribute("authors", authors);
        request.setAttribute("types", types);
        this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/catalog.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer author_id = (!request.getParameter("author").isEmpty())  ? Integer.parseInt(request.getParameter("author")) : null;
        Integer type_id = (!request.getParameter("type").isEmpty()) ? Integer.parseInt(request.getParameter("type")) : null;
        Boolean available = (!request.getParameter("availability").isEmpty()) ? Boolean.parseBoolean(request.getParameter("availability")) : null;

        CatalogDAO catalogDAO = new CatalogDAO();
        AuthorDAO authorDAO = new AuthorDAO();
        TypeDAO typeDAO = new TypeDAO();

        List<com.tp5.models.Catalog> catalog = catalogDAO.getWithFilters(author_id, type_id, available);
        List<Author> authors = authorDAO.getAll();
        List<Type> types = typeDAO.getAll();

        request.setAttribute("title", "Catalogue");
        request.setAttribute("catalog", catalog);
        request.setAttribute("authors", authors);
        request.setAttribute("types", types);
        this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/catalog.jsp").forward(request, response);
    }
}
