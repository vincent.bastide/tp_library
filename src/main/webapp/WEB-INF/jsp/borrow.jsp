<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <c:choose>
                <c:when test="${book_id == null}">
                    <h1>Erreur : Le livre sélectionné n'est pas disponible</h1>
                    <a href="/catalog">Retourner au catalogue</a>
                </c:when>
                <c:otherwise>
                    <h1>Confirmation de l'emprunt</h1>
                    <p>Êtes-vous sûr de vouloir emprunter <c:out value="${book.getTitle()}"/> ?</p>
                    <form action="/borrow" method="post">
                        <input type="hidden" name="book_id" value="<c:out value="${book_id}"/>">
                        <input type="submit" value="Valider">
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </jsp:body>
</layout:layout>