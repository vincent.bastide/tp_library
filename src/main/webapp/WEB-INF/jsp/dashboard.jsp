<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <h1>Espace personnel</h1>
            <div class="dashboard">
                <div class="sub-content">
                    <table>
                        <tr>
                            <th>Titre</th>
                            <th>Auteur</th>
                            <th>Date emprunt</th>
                            <th>Statut</th>
                            <th>Action</th>
                        </tr>
                        <c:forEach items="${borrowings}" var="borrowing">
                            <tr>
                                <td><c:out value="${borrowing.getBook().getTitle()}"/></td>
                                <td><c:out value="${borrowing.getBook().getAuthor()}"/></td>
                                <td><c:out value="${borrowing.getBorrowed()}"/></td>
                                <td>#TO_DEFINE</td>
                                <td><a href="/return/<c:out value="${borrowing.getBook().getId()}"/>">Rendre</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </jsp:body>
</layout:layout>