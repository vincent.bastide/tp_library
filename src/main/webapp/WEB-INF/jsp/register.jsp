<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <h1>Créer un compte</h1>
            <form id="registerForm" action="/register" method="post">
                <div class="form_group">
                    <input name="lastname" type="text" placeholder="Nom*" value="${lastname}" required>
                </div>
                <div class="form_group">
                    <input name="firstname" type="text" placeholder="Prénom*" value="${firstname}" required>
                </div>
                <div class="form_group">
                    <input name="email" type="text" placeholder="Email*" value="${email}" required>
                </div>
                <div class="form_group">
                    <input id="password" name="password" type="password" placeholder="Mot de passe*" minlength="4" required>
                </div>
                <div class="form_group">
                    <input id="confirmed_password" name="confirmed_password" type="password" placeholder="Confirmation mot de passe*" minlength="4" required>
                </div>
                <div class="form_group">
                    <button type="submit" value="Créer un compte">Créer un compte</button>
                </div>

            </form>
        </div>
    </jsp:body>
</layout:layout>