<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <h1>Se connecter</h1>
            <form id="registerForm" action="/login" method="post">
                <div class="form_group">
                    <input name="email" type="text" placeholder="Email*" value="${email}" required>
                </div>
                <div class="form_group">
                    <input name="password" type="password" placeholder="Mot de passe*" required>
                </div>
                <div class="form_group">
                    <button type="submit" value="Créer un compte">Se connecter</button>
                </div>
            </form>
        </div>l
    </jsp:body>
</layout:layout>