<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <h1>Catalogue</h1>
            <c:if test="${catalog != null}">
                <form id="research_form" action="/catalog" method="post">
                    <div class="form_group">
                        <label>Auteur</label>
                        <select name="author" id="author">
                            <option value="" selected="selected">auteur</option>
                            <c:forEach items="${authors}" var="author">
                                <option value="${author.getId()}"><c:out value="${author.getFullName()}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form_group">
                        <label>Genre</label>
                        <select name="type" id="type">
                            <option value="" selected="selected">Genre</option>
                            <c:forEach items="${types}" var="type">
                                <option value="${type.getId()}"><c:out value="${type.getName()}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form_group">
                        <label>Disponibilité</label>
                        <select name="availability" id="availability">
                            <option value="" selected="selected">Tout</option>
                            <option value="true">Disponible</option>
                            <option value="false">Non disponible</option>
                        </select>
                    </div>
                    <button type="submit">Rechercher</button>
                </form>
                <table>
                    <tr>
                        <th>Titre</th>
                        <th>Genre</th>
                        <th>Auteur</th>
                        <th>Disponibilité</th>
                        <c:if test="${isConnected != null}">
                            <th></th>
                        </c:if>
                    </tr>
                    <c:forEach items="${catalog}" var="row">
                        <tr>
                            <td><c:out value="${row.getBook().getTitle()}"/></td>
                            <td><c:out value="${row.getBook().getType().getName()}"/></td>
                            <td><c:out value="${row.getBook().getAuthor().getFullName()}"/></td>
                            <td><c:out value="${row.getNbAvailable()}"/></td>
                            <c:if test="${isConnected != null}">
                                <td><a href="/borrow?id=<c:out value="${row.getBook().getId()}"/>">Emprunter</a></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>

        </div>
    </jsp:body>
</layout:layout>