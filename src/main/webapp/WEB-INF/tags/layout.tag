<%@tag description="Base page layout" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oswald|Source+Sans+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/index.css" type="text/css">
    <title>${title}</title>
</head>
<body>
<div class="container">
    <!-- Register/Login buttons -->
    <div class="member_btn">
        <c:choose>
            <c:when test="${isConnected == null}">
                <a href="/login">Se connecter</a>
                <a href="/register">Créer un compte</a>
            </c:when>
            <c:otherwise>
                <a href="/dashboard">Espace perso</a>
                <a href="/logout">Se déconnecter</a>
            </c:otherwise>
        </c:choose>
    </div>
    <header>
        <div class="home_title">
            <h1>Bibliothèque</h1>
        </div>
        <nav>
            <a href="/">Accueil</a>
            <a href="/catalog">Catalogue</a>
            <a href="/about">A propos</a>
            <a href="/contact">Contact</a>
        </nav>
    </header>
    <div class="content">
        <jsp:doBody />
    </div>
</div>
</body>
</html>